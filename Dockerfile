# Docker image
FROM node:10

# Ensure "node_modules" are created before "npm i" with "node:node" ownership
RUN mkdir -p ~/www

# Setting working directory
WORKDIR ~/www

# Copy "package.json" and "package-lock.json"
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy current application code into working directory
COPY . .
RUN npm run build

# Expose 3000 port
EXPOSE 3000

CMD ["npm", "start"]

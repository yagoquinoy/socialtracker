import React from 'react';
import PropTypes from 'prop-types';

import { Icon } from '../Icons/Icon';
import TweetModal from '../TweetModal';

class CommentsTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false,
      comment: {},
    };

    this.getHighestEmotion = this.getHighestEmotion.bind(this);
    this.renderSelectOptions = this.renderSelectOptions.bind(this);
    this.renderComments = this.renderComments.bind(this);
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  getHighestEmotion(emotions) {
    let highestEmotion = '';
    let highestLevel = 0;

    for (let emotion in emotions) {
      if (emotions[emotion] > highestLevel) {
        highestLevel = emotions[emotion];
        highestEmotion = emotion;
      }
    }

    return highestEmotion;
  }

  openModal(comment) {
    this.setState({
      modalIsOpen: true,
      comment,
    });
  }

  closeModal() {
    this.setState({
      modalIsOpen: false,
      comment: {},
    });
  }

  renderSelectOptions(options, type = 'option') {
    return options.map(option => (
      <option key={`${type}-${option}`} value={option}>
        {option}
      </option>
    ));
  }

  renderComments(comments, limit) {
    if (limit !== undefined) {
      comments = comments.slice(0, limit);
    }

    return comments.map(comment => (
      <tr
        className="comments-table__tr"
        key={comment.id_str}
        onClick={() => this.openModal(comment)}
      >
        <td>{new Date(comment.date).toLocaleString()}</td>
        <td>{comment.user.location}</td>
        <td>
          <img src={comment.user.profile_image_url_https} />@
          {comment.user.screen_name}
        </td>
        <td>
          <Icon customStyles={{ fontSize: '40px' }} iconName={this.getHighestEmotion(comment.emotion)} />
        </td>
        <td>
          {comment.text}
        </td>
      </tr>
    ));
  }

  render() {
    const { limit, list } = this.props;
    const { comment, modalIsOpen } = this.state;

    return (
      <div className="comments-table">
        <h2 className="comments-table__title">Tweets</h2>
        <table className="comments-table__table">
          <thead className="comments-table__thead">
            <tr>
              <th>Date</th>
              <th>Place</th>
              <th>Author</th>
              <th>Emotion</th>
              <th>Comment</th>
            </tr>
          </thead>
          <tbody>{this.renderComments(list, limit)}</tbody>
        </table>
        {modalIsOpen && <TweetModal data={comment} onClose={this.closeModal} />}
      </div>
    );
  }
}

CommentsTable.propTypes = {
  keywords: PropTypes.array.isRequired,
  entities: PropTypes.array.isRequired,
  list: PropTypes.array.isRequired,
};

export default CommentsTable;

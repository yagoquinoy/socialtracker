import React from 'react';
import PropTypes from 'prop-types';

const styles = {
  padding: '0 0 1em',
};

// Fak register due to time
class TwoFactorForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = { code: '' };

    this.handleCodeChange = this.handleCodeChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleCodeChange(event) {
    this.setState({ code: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.updateData();
  }

  render() {
    return (
      <div>
        <form style={styles} onSubmit={this.handleSubmit}>
          <div>
            <label>Code</label>
            <input
              type="text"
              name="code"
              value={this.state.code}
              onChange={this.handleCodeChange}
            />
          </div>
          <div>
            <button type="submit">Verify</button>
          </div>
        </form>
      </div>
    );
  }
}

TwoFactorForm.propTypes = {
  updateData: PropTypes.func,
};

export default TwoFactorForm;

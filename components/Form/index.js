import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';

const styles = {
  padding: '0 0 1em',
};

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: '', error: false };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.clean = this.clean.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value, error: false });
  }

  handleSubmit(event) {
    event.preventDefault();

    // this.setState({
    //   error: true,
    // });

    // Request and then...
    axios
      .post('/api/twitter', {
        url: this.state.value,
      })
      .then(response => {
        console.log(response);
        this.props.updateData(response.data);
      })
      .catch(error => {
        console.error(error);
        this.setState({
          error: true,
        });
      });

    // this.props.updateData(data);
  }

  clean() {
    this.setState({ value: '', error: false });
  }

  render() {
    const displayNone = { display: 'none' };
    return (
      <div className="search-form">
        <h1 className="title">Let's get started</h1>
        <form
          className="search-form__form"
          style={styles}
          onSubmit={this.handleSubmit}
        >
          <input
            className="search-form__input"
            placeholder="TYPE AN URL OR A TEXT"
            type="text"
            value={this.state.value}
            onChange={this.handleChange}
          />
          <button className="search-form__button" type="submit">
            Enviar
          </button>
          <button style={displayNone} type="button" onClick={this.clean}>
            Limpiar
          </button>
          {this.state.error && <p>Por favor, introduce la URL de un tweet</p>}
        </form>
      </div>
    );
  }
}

Form.propTypes = {
  updateData: PropTypes.func,
};

export default Form;

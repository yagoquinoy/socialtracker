import React from 'react';

const defaultStyles = {
    display: 'inline-block',
    width: '1em',
    height: '1em',
    strokeWidth: 0,
    stroke: 'currentColor',
    fill: 'currentColor',
};

const Icon = ({ iconName, customStyles }) => {
    const id = `#icon-ico-${iconName}`;
    const styles = Object.assign({}, defaultStyles, customStyles);

    return (
        <svg style={styles}><use xlinkHref={id}></use></svg>
    );
};

export { Icon };

import React from 'react';

const wrapperStyles = {
  position: 'relative',
  borderRadius: '16px',
  backgroundColor: 'rgba(103, 52, 7, 0.24)',
  color: 'rgba(103, 52, 7, 0.48)',
};

const labelValueStyles = {
  position: 'absolute',
  width: '100%',
  borderRadius: '16px',
  display: 'flex',
  justifyContent: 'space-between',
  fontSize: '18px',
};

const backgroundWrapperStyle = {
  display: 'flex',
  justifyContent: 'space-between',
};

const backgroundStyle = {
  borderRadius: '16px',
  backgroundColor: '#fae1a9',
  height: '32px',
  width: '64px',
};

const spanStyle = {
  padding: '8px 16px',
  height: '32px',
  display: 'inline-flex',
  alignItems: 'center',
};

const LabelValue = ({ label, value, width = 0, ratio = 1, color }) => {
  const labelBackgroundStyle = { ...backgroundStyle };
  const valueBackgroundStyle = { ...backgroundStyle };

  let widthStyle;
  if (width < 10) {
    widthStyle = '16px';
  } else {
    widthStyle = `${width * ratio}%`;
  }

  if (color) {
    labelBackgroundStyle.backgroundColor = color;
    valueBackgroundStyle.backgroundColor = color;
  }

  labelBackgroundStyle.width = widthStyle;

  return (
    <div style={wrapperStyles}>
      <div style={labelValueStyles}>
        <span style={spanStyle}>{label}</span>
        <span style={spanStyle}>{value}</span>
      </div>
      <div style={backgroundWrapperStyle}>
        <div style={labelBackgroundStyle} />
        <div style={valueBackgroundStyle} />
      </div>
    </div>
  );
};

export default LabelValue;

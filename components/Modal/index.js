import ReactModal from 'react-modal';
ReactModal.setAppElement('#__next');

import { Icon } from '../Icons/Icon';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    borderRadius: '24px',
    backgroundColor: 'rgba(255, 255, 255, 0.88)',
  },
  overlay: {
    backgroundColor: 'rgba(103, 52, 7, 0.32)',
  },
};

const wrapperStyles = {
  minWidth: '508px',
  maxHeight: '600px',
};

const actionsWrapperStyles = {
  marginBottom: '16px',
};

import React, { Component } from 'react';

class Modal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modalIsOpen: true,
    };

    this.closeModal = this.closeModal.bind(this);
  }

  closeModal() {
    const { onClose } = this.props;
    this.setState({ modalIsOpen: false }, () => onClose());
  }

  render() {
    const { children, onOpen } = this.props;
    const { modalIsOpen } = this.state;

    return (
      <ReactModal
        isOpen={modalIsOpen}
        onAfterOpen={onOpen}
        style={customStyles}
      >
        <button className="modal__button" onClick={this.closeModal}>
          <Icon iconName="close" customStyles={{ fontSize: '24px' }} />
        </button>
        <div className="modal" style={wrapperStyles}>
          <div style={actionsWrapperStyles}>
          </div>
          {children}
        </div>
      </ReactModal>
    );
  }
}

export default Modal;

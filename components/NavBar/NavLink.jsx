import React, { Component } from 'react';
import Router from 'next/router';

import { Icon } from '../Icons/Icon';

class NavLink extends Component {
    constructor(props) {
        super(props);
        this.state = { isActiveLink: false };

        this.isActiveLink = this.isActiveLink.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    componentDidMount() {
        if (Router.route === this.props.route) {
            this.setState({
                isActiveLink: true
            });
        }
    }

    handleClick() {
        Router.push(this.props.route);
    }

    isActiveLink() {
        return this.state.isActiveLink;
    }

    render() {
        let className = 'navbar__navlink';

        if (this.isActiveLink()) {
            className += ' active';

        }

        return (
            <button className={className} onClick={this.handleClick}>
                <Icon {...this.props} />
            </button>
        );
    }
}

export { NavLink };

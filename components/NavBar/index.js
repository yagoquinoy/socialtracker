import React from 'react';
import { NavLink } from './NavLink';

const NavBar = () => {
  const styles = {
    display: 'flex',
    flexDirection: 'column',
    width: '64px',
    height: '100vh',
    padding: '36px 12px',
    background: '#fae1a9',
  };

  const iconStyle = {
    fontSize: '40px',
  };

  return (
    <nav className="navbar" style={styles}>
      <NavLink customStyles={iconStyle} iconName="search" route="/twitter" />
      <NavLink customStyles={iconStyle} iconName="history" route="/history" />
      <NavLink customStyles={iconStyle} iconName="user" route="/user" />
      <NavLink
        customStyles={iconStyle}
        iconName="notifications"
        route="/notifications"
      />
    </nav>
  );
};

export default NavBar;

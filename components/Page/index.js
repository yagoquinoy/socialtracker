import React from 'react';
import Head from 'next/head';

import NavBar from '../NavBar';
import Icons from '../Icons';

import '../../static/styles.scss';

const Page = ({ hideNavBar, children }) => {
  return (
    <div>
      <Head>
        <title>Social Tracker</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link
          href="https://fonts.googleapis.com/css?family=Pathway+Gothic+One"
          rel="stylesheet"
        />
        <link rel="stylesheet" href="/_next/static/style.css" />
      </Head>
      <Icons />
      <div className="container">
        {!hideNavBar && <NavBar />}
        <main className="content">{children}</main>
      </div>
    </div>
  );
};

export default Page;

import React from 'react';

const seeMoreWrapperStyle = {
  position: 'absolute',
  alignItems: 'center',
  backgroundColor: 'rgba(98, 148, 96, 0.24)',
  borderBottomLeftRadius: '24px',
  borderBottomRightRadius: '24px',
  bottom: 0,
  display: 'flex',
  fontSize: '14px',
  height: '56px',
  justifyContent: 'center',
  letterSpacing: '3px',
  width: '100%',
  cursor: 'pointer',
};

const seeMoreStyles = {
  color: '#243119',
};

const SeeMore = ({ onClick }) => (
  <div
    style={seeMoreWrapperStyle}
    onClick={event => {
      event.preventDefault();
      onClick();
    }}
  >
    <span style={seeMoreStyles} href="">
      SEE MORE
    </span>
  </div>
);

export default SeeMore;

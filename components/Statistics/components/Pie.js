import React from 'react';
import { Pie } from 'react-chartjs';

const options = {
  responsive: true,
  maintainAspectRatio: false,
  scales: {
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
        },
      },
    ],
  },
};

const wrapperStyles = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: '16px',
};

const titleStyles = {
  fontSize: '24px',
  color: '#673407',
  textAlign: 'center',
  margin: '0 0 10px 0',
};

const pieWrapperStyles = {
  height: '195px',
  width: '232px',
  marginBottom: '16px',
};

const liStyles = {
  display: 'flex',
  marginBottom: '8px',
  fonstSize: '18px',
  color: 'rgba(103, 52, 7, 0.48)',
};

const squareStyles = {
  height: '24px',
  marginRight: '16px',
  width: '24px',
};

const ulStyle = {
  width: '100%',
};

const WrappedPie = ({ data, title }) => {
  return (
    <div style={wrapperStyles}>
      <p style={titleStyles}>{title}</p>
      <div style={pieWrapperStyles}>
        <Pie data={data} options={options} />
      </div>
      <ul style={ulStyle}>
        {data.map(item => (
          <li style={liStyles} key={item.label}>
            <div style={{ ...squareStyles, backgroundColor: item.color }} />
            <label>{item.label}</label>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default WrappedPie;

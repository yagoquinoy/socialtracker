import React, { Component } from 'react';

import { sorter } from '../../utils';

import Worldmap from '../Worldmap';
import SeeMore from '../SeeMore';
import WordsModal from '../WordsModal';
import Pie from './components/Pie';

const MAX_PIE = 4;

const markers = [
  { markerOffset: 0, coordinates: [-58.3816, -34.6037], r: 16 }, // Buenos Aires
  { markerOffset: 0, coordinates: [-3.70379, 40.416775], r: 24 }, // Madrid
  { markerOffset: 0, coordinates: [114.174637, 22.302219], r: 32 }, // Hong Kong
  { markerOffset: 0, coordinates: [-73.90242, 40.73061], r: 22 }, // New York
  { markerOffset: 0, coordinates: [28.034088, -26.195246], r: 28 }, // Johannesburg
  { markerOffset: 0, coordinates: [13.404954, 52.520008], r: 40 }, // Berlin
];

const wrapperStyle = {
  position: 'relative',
  background: '#FFF',
  height: '468px',
  borderRadius: '24px',
  boxShadow:
    '0 4px 5px 0 rgba(0, 0, 0, 0.2), 0 3px 14px 3px rgba(0, 0, 0, 0.12), 0 8px 10px 1px rgba(0, 0, 0, 0.14)',
  marginRight: '24px',
};

const pieWrapperStyle = {
  ...wrapperStyle,
  width: '252px',
};

const staticsWrapperStyles = {
  display: 'flex',
  marginBottom: '24px',
};

const worldmapStyles = { flex: 1 };

const colors = [
  {
    color: '#FAC46E',
    highlight: '#FFDE88',
  },
  {
    color: '#fcffcc',
    highlight: '#FFFFE6',
  },
  {
    color: '#629460',
    highlight: '#7CAE7A',
  },
  {
    color: '#CDD185',
    highlight: '#E7EB9F',
  },
];

function removeAuthorFromEntities(author) {
  return word => word[0] !== `@${author}`;
}

function filterMentionsFromKeywords(word) {
  return word[0].charAt(0) !== '@';
}

function mapMostRepeatedData(sortedWords) {
  const mostRepeated = [];

  let i = 0;
  while (mostRepeated.length < MAX_PIE && i < sortedWords.length) {
    const word = sortedWords[i];
    mostRepeated.push({
      label: word[0],
      value: word[1],
      color: colors[i].color,
      highlight: colors[i].highlight,
    });
    i++;
  }

  return mostRepeated;
}

class Statistics extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showKeywordsModal: false,
      showEntitiesModal: false,
    };

    this.toggleKeywordsModal = this.toggleKeywordsModal.bind(this);
    this.toggleEntitiesModal = this.toggleEntitiesModal.bind(this);
  }

  toggleKeywordsModal() {
    const { showKeywordsModal } = this.state;
    this.setState({ showKeywordsModal: !showKeywordsModal });
  }

  toggleEntitiesModal() {
    const { showEntitiesModal } = this.state;
    this.setState({ showEntitiesModal: !showEntitiesModal });
  }

  render() {
    const { author, keywords, entities } = this.props;
    const { showKeywordsModal, showEntitiesModal } = this.state;

    const sortedKeywords = sorter(keywords).filter(filterMentionsFromKeywords);
    const sortedEntities = sorter(entities).filter(
      removeAuthorFromEntities(author),
    );

    const mostRepeatedKeywords = mapMostRepeatedData(sortedKeywords);
    const mostRepeatedEntities = mapMostRepeatedData(sortedEntities);

    return (
      <div style={staticsWrapperStyles}>
        <div style={pieWrapperStyle}>
          <Pie data={mostRepeatedKeywords} title="Keywords" />
          <SeeMore onClick={this.toggleKeywordsModal} />
          {showKeywordsModal && (
            <WordsModal
              data={sortedKeywords}
              title="Keyword"
              onClose={this.toggleKeywordsModal}
            />
          )}
        </div>
        <div style={pieWrapperStyle}>
          <Pie data={mostRepeatedEntities} title="Entities" />
          <SeeMore onClick={this.toggleEntitiesModal} />
          {showEntitiesModal && (
            <WordsModal
              data={sortedEntities}
              title="Entity"
              onClose={this.toggleEntitiesModal}
            />
          )}
        </div>
        <div style={Object.assign({}, wrapperStyle, { marginRight: 0 })}>
          <Worldmap
            className="worldmap"
            markers={markers}
            style={worldmapStyles}
          />
          <SeeMore onClick={() => {}} />
        </div>
      </div>
    );
  }
}

export default Statistics;

import React from 'react';

const Author = ({ screenName, image }) => (
  <p className="tweet-modal__element tweet-modal-author">
    <span className="tweet-modal__label tweet-modal-author__label">Author</span>
    <img className="tweet-modal-author__image" src={image} />
    <span className="tweet-modal__text-value tweet-modal-author__name">@{screenName}</span>
  </p>);

export default Author;

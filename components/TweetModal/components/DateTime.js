import React from 'react';

const DateTime = ({ datetime }) => (
  <p className="tweet-modal__element">
    <span className="tweet-modal__label">Date & Time</span>
    <span className="tweet-modal__text-value">{new Date(datetime).toLocaleString()}</span>
  </p>);

export default DateTime;

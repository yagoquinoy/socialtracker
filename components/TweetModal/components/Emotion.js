import React from 'react';
import LabelValue from '../../LabelValue';
import { Icon } from '../../Icons/Icon';

const Emotion = ({ emotions }) => (
  <div className="tweet-modal__element tweet-modal__element--margin-bottom">
    <p className="tweet-modal__label">Emotion</p>
    <div className="tweet-modal__emotion-bars">
      {Object.keys(emotions).map(emotion => {
        const width = Math.round(emotions[emotion] * 100);
        return (
          <div key={emotion} className="tweet-modal__emotion-bar-container">
            <Icon iconName={emotion} customStyles={{ fontSize: '56px' }} />
            <div className="tweet-modal__emotion-bar">
              <LabelValue
                label={emotion}
                value={`${width} %`}
                width={width}
                color="#fcffcc"
              />
            </div>
          </div>
        );
      })}
    </div>
  </div>
);

export default Emotion;

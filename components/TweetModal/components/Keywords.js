import React from 'react';

import LabelValue from '../../LabelValue';

const wrapperStyles = {};

const flexWrapperStyles = {
  display: 'flex',
  marginBottom: '32px'
};

const fixedWidthStyle = {
  width: '80px',
};

const flexItemStyles = {
  flex: 1,
};

const liStyles = {
  marginBottom: '16px',
};

const sorter = (entry1, entry2) => {
  if (entry1.relevance > entry2.relevance) {
    return -1;
  }

  if (entry1.relevance < entry2.relevance) {
    return 1;
  }

  return 0;
};

const getWidth = relevance => {
  if (relevance < 0.1) {
    return 0;
  }

  return Math.round(relevance * 100);
};

const Keywords = ({ keywords = [], title }) => {
  const firstFive = keywords.sort(sorter).slice(0, 5);

  return (
    <div style={wrapperStyles}>
      <p className="tweet-modal__label tweet-modal__label--margin-bottom">{title}</p>
      <div style={flexWrapperStyles}>
        <div style={fixedWidthStyle} />
        <div style={flexItemStyles}>
          <ul>
            {firstFive &&
              firstFive.length > 0 &&
              firstFive.map(word => {
                const percentage = `${Math.round(word.relevance * 100)} %`;
                return (
                  <li style={liStyles} key={word.text}>
                    <LabelValue
                      label={word.text}
                      value={percentage}
                      width={getWidth(word.relevance)}
                    />
                  </li>
                );
              })}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Keywords;

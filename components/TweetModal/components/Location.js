import React from 'react';

const Location = ({ location }) => (
  <p className="tweet-modal__element">
    <span className="tweet-modal__label">Location</span>
    <span className="tweet-modal__text-value">{location || '-'}</span>
  </p>);

export default Location;

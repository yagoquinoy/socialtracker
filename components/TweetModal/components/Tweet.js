import React from 'react';

const Tweet = ({ comment }) => (
  <p className="tweet-modal__element">
    <span className="tweet-modal__label">Comment</span>
    <span className="tweet-modal__text-value">{comment}</span>
  </p>);

export default Tweet;

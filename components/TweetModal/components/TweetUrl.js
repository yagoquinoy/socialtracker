import React from 'react';

const TweetUrl = ({ url }) => (
  <p className="tweet-modal__element">
    <span className="tweet-modal__label">Url</span>
    <a className="tweet-modal__text-value" href={url} target="_blank">{url}</a>
  </p>);

export default TweetUrl;

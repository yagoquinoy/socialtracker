import React from 'react';

import Modal from '../Modal';

import Author from './components/Author';
import DateTime from './components/DateTime';
import Emotion from './components/Emotion';
import Keywords from './components/Keywords';
import Location from './components/Location';
import Tweet from './components/Tweet';
import TweetUrl from './components/TweetUrl';

const wrapperStyles = {
  width: '87%',
  maxWidth: '1264px',
  height: '813px',
};

const flexWrapperStyles = {
  display: 'flex',
};

const flexItemStyles = {
  flex: 1,
};

const fixedWidthStyles = {
  width: '50%',
  maxWidth: '720px',
};

const entitiesWrapperStyles = {
  ...fixedWidthStyles,
  display: 'flex',
};

const entitiesWrapper = {
  flexGrow: 1,
  display: 'inline-block',
  maxWidth: '512px',
  width: '35%',
};

const keywordsStyles = {
  flexGrow: 1,
  maxWidth: '512px',
  width: '35%',
};

const TweetModal = ({ data, onClose }) => {
  const categories = data.categories || [];

  const mappedCategories = categories.map(({ label, score }) => ({
    text: label,
    relevance: score,
  }));

  return (
    <Modal onClose={onClose}>
      <div style={wrapperStyles}>
        <div style={flexWrapperStyles}>
          <div style={flexItemStyles}>
            <Author
              screenName={data.user.screen_name}
              image={data.user.profile_image_url_https}
            />
          </div>
          <div style={fixedWidthStyles}>
            <Tweet comment={data.text} />
          </div>
        </div>
        <div style={flexWrapperStyles}>
          <div style={flexItemStyles}>
            <DateTime datetime={data.date} />
          </div>
          <div style={fixedWidthStyles}>
            <TweetUrl url={data.url} />
          </div>
        </div>
        <Location location={data.user.location} />
        <Emotion emotions={data.emotion} />
        <Keywords
          title="Posibilities to belong to a Category"
          keywords={mappedCategories}
        />
        <div style={flexWrapperStyles}>
          <div style={keywordsStyles}>
            <Keywords title="Keywords relevance" keywords={data.keywords} />
          </div>
          <div style={entitiesWrapperStyles}>
            <div style={entitiesWrapper}>
              <Keywords title="Entities relevance" keywords={data.entities} />
            </div>
          </div>
        </div>
      </div>
    </Modal>
  );
};

export default TweetModal;

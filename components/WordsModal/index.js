import React, { Component } from 'react';

import Modal from '../Modal';
import LabelValue from '../LabelValue';

const titleStyles = {
  color: '#673407',
  fontSize: '24px',
  display: 'flex',
  justifyContent: 'space-between',
  marginBottom: '16px',
};

const liStyles = {
  marginBottom: '16px',
};

class WordsModal extends Component {
  getWidth(total, value) {
    if (value === 1) {
      return 0;
    }

    return Math.round((value / total) * 100);
  }

  render() {
    const { data = [], title, onClose } = this.props;

    const total = data[0][1];

    return (
      <Modal onClose={onClose}>
        <div>
          <div style={titleStyles}>
            <span>{title}</span>
            <span>Times repeated</span>
          </div>
        </div>
        <ul>
          {data.map(word => (
            <li style={liStyles} key={word[0]}>
              <LabelValue
                label={word[0]}
                value={word[1]}
                width={this.getWidth(total, word[1])}
                ratio={0.5}
              />
            </li>
          ))}
        </ul>
      </Modal>
    );
  }
}

export default WordsModal;

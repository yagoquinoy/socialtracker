import React, { Component } from 'react';
import {
  ComposableMap,
  ZoomableGroup,
  Geographies,
  Geography,
  Markers,
  Marker,
} from 'react-simple-maps';

const ground = '#FAE1A9';
const stroke = '#FFFFFF';

const geographyStyle = {
  default: {
    fill: ground,
    stroke,
    strokeWidth: 1,
    outline: 'none',
  },
  hover: {
    fill: '#CFD8DC',
    stroke,
    strokeWidth: 1,
    outline: 'none',
  },
  pressed: {
    fill: ground,
    stroke,
    strokeWidth: 1,
    outline: 'none',
  },
};

const markerColor = '#629460';
const markerColorHover = '#629460';
const markerStyle = {
  default: { fill: markerColor },
  hover: { fill: markerColorHover },
  pressed: { fill: markerColor },
};

const markerCircleStyle = {
  stroke: '#00000',
  strokeWidth: 8,
  opacity: 0.6,
};

const markerTextStyle = {
  fontFamily: 'Roboto, sans-serif',
  fill: '#607D8B',
};

class Worldmap extends Component {
  constructor() {
    super();

    this.state = {
      zoom: 1,
    };

    this.handleZoomIn = this.handleZoomIn.bind(this);
    this.handleZoomOut = this.handleZoomOut.bind(this);
  }

  handleZoomIn() {
    const newZoom = this.state.zoom * 2;
    const newState = { zoom: newZoom <= 16 ? newZoom : 16 };
    this.setState(newState);
  }

  handleZoomOut() {
    const newZoom = this.state.zoom / 2;
    const newState = { zoom: newZoom >= 1 ? newZoom : 1 };
    this.setState(newState);
  }

  wheel(e) {
    e.preventDefault();
    const handler = e.deltaY < 0 ? this.handleZoomIn : this.handleZoomOut;
    handler();
  }

  render() {
    const { className, markers } = this.props;
    return (
      <div className={className} onWheel={e => this.wheel(e)}>
        <ComposableMap height={468}>
          <ZoomableGroup zoom={this.state.zoom}>
            <Geographies geography={'/static/world-50n.json'}>
              {(geographies, projection) =>
                geographies.map((geography, key) => {
                  return (
                    <Geography
                      key={key}
                      geography={geography}
                      projection={projection}
                      style={geographyStyle}
                    />
                  );
                })
              }
            </Geographies>
            <Markers>
              {markers.map(({ r, ...marker }, i) => (
                <Marker key={i} marker={marker} style={markerStyle}>
                  <circle cx={0} cy={0} r={r} style={markerCircleStyle} />
                  <text
                    textAnchor="middle"
                    y={marker.markerOffset}
                    style={markerTextStyle}
                  >
                    {marker.name}
                  </text>
                </Marker>
              ))}
            </Markers>
          </ZoomableGroup>
        </ComposableMap>
      </div>
    );
  }
}

export default Worldmap;

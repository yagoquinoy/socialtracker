module.exports = {
  MONGO_URL: 'mongodb://mongo:27017/test',
  NLU_VERSION: '2018-04-05',
  TWITTER_URL: 'https://twitter.com/',
  TWITTER_SEARCH_TWEETS_ENDPOINT: 'search/tweets',
  TWEET_LIMIT: 50,
  TWEET_MODE: 'extended',
  TWEET_RESULT_TYPE: 'mixed',
  TWEET_INCLUDE_ENTITIES: true,
  MAX_PAGES: 3,
};

const withSass = require('@zeit/next-sass');
module.exports = withSass({
  onDemandEntries: {
    websocketPort: 41963,
  },
});

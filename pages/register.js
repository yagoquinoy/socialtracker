import React, { Component } from 'react';
import Link from 'next/link';

import RegisterForm from '../components/Form/RegisterForm';

const wrapperStyles = {
  margin: '0 auto',
  width: '70%',
  maxWidth: '980px',
};

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = { showQr: false };

    this.updateData = this.updateData.bind(this);
  }

  updateData() {
    this.setState({ showQr: true });
  }

  render() {
    return (
      <section>
        <div style={wrapperStyles}>
          <RegisterForm updateData={this.updateData} />
          {this.state.showQr && (
            <div>
              <img src="/_next/static/qr.png" />
              <Link href="/">Ir a la página de login</Link>
            </div>
          )}
        </div>
      </section>
    );
  }
}

export default Register;

import React, { Component } from 'react';
import Form from '../components/Form';
import Statistics from '../components/Statistics';
import CommentsTable from '../components/CommentsTable';
import Page from '../components/Page';

export default class Twitter extends Component {
  constructor(props) {
    super(props);
    this.state = { data: [], showGraphs: false };
    this.updateData = this.updateData.bind(this);
  }

  updateData(data) {
    this.setState({ data, showGraphs: true });
  }

  render() {
    const { author, text, comments, statistics } = this.state.data;

    const statisticsTitle = author || text;

    return (
      <Page>
        {!this.state.showGraphs && <Form updateData={this.updateData} />}
        {this.state.showGraphs && (
          <div className="results-page">
            <h1 className="results-page__title title">Results</h1>
            <Statistics
              keywords={statistics.keywords}
              entities={statistics.entities}
              author={statisticsTitle}
            />
            <CommentsTable
              keywords={Object.keys(statistics.keywords)}
              entities={Object.keys(statistics.entities)}
              emotions={[]}
              list={comments}
            />
          </div>
        )}
      </Page>
    );
  }
}

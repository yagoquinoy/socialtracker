import React, { Component } from 'react';
import Router from 'next/router';

import TwoFactorForm from '../components/Form/TwoFactorForm';

const wrapperStyles = {
  margin: '0 auto',
  width: '70%',
  maxWidth: '980px',
};

class TwoFactor extends Component {
  constructor(props) {
    super(props);

    this.updateData = this.updateData.bind(this);
  }

  updateData() {
    Router.push('/twitter');
  }

  render() {
    return (
      <section>
        <div style={wrapperStyles}>
          <TwoFactorForm updateData={this.updateData} />
        </div>
      </section>
    );
  }
}

export default TwoFactor;

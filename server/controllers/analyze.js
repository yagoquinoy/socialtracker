const analyze = require('../logic/analyze');
const constants = require('../../constants');

const TWEET_REGEX = new RegExp(
  /^https?:\/\/twitter\.com\/(?:#!\/)?(\w+)\/status(es)?\/(\d+)$/,
); // fragment locator

function isTwitterURL(str) {
  return TWEET_REGEX.test(str);
}

function getComments(text) {
  const [author, tweetId] = text
    .replace(constants.TWITTER_URL, '')
    .split('/status/');

  // TODO: Validate author and tweetId is valid

  console.log(`POST analyze tweet comments by ${author} ${tweetId}`);

  return analyze.findAndAnalyzeTweetComments(author, tweetId);
}

function getTweets(text) {
  console.log(`POST analyze tweets that contains ${text}`);

  return analyze.findAndAnalyzeTweetsByText(text);
}

function twitter(req, res) {
  // TODO: Cambiar url por algo más significativo?
  const text = req.body.url;

  const p = isTwitterURL(text) ? getComments(text) : getTweets(text);

  p.then(result => res.json(result)).catch(e => {
    console.error(e);
    res.status(500);
  });
}

module.exports = {
  twitter,
};

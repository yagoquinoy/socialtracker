const userLogic = require('../logic/user');

function register(req, res) {
  const { email, password } = req.body;
  userLogic
    .register(email, password)
    .then(user => {
      res.send({ user });
    })
    .catch(e => {
      console.error(e);
      res.status(500);
    });
}

function login(req, res) {
  const { email, password } = req.body;
  userLogic
    .login(email, password)
    .then(token => {
      res.send({ token });
    })
    .catch(e => {
      console.error(e);
      res.status(500);
    });
}

function verify(req, res) {
  res.status(200);
}

module.exports = {
  register,
  login,
  verify,
};

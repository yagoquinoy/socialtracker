const mongoose = require('mongoose');
const express = require('express');
const bodyParser = require('body-parser');

const C = require('../constants');
const router = require('./router');

mongoose.set('useCreateIndex', true);

const port = parseInt(process.env.PORT, 10) || 3000;

module.exports = function(app, handle) {
  const server = express(app);
  //support parsing of application/json type post data
  server.use(bodyParser.json());

  //support parsing of application/x-www-form-urlencoded post data
  server.use(bodyParser.urlencoded({ extended: true }));

  router({ app, handle, server });

  mongoose.connect(C.MONGO_URL);

  server.listen(port, err => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${port}`);
  });
};

const moment = require('moment');
const C = require('../../constants');
const TweetCommentsStats = require('../models/TweetCommentsStats');
const TwitterTextSearchStats = require('../models/TwitterTextSearchStats');
const { decrementHugeNumberBy1 } = require('../utils/arithmetic');
const twitterLogic = require('./twitter');
const nlu = require('./nlu');

const { MAX_PAGES } = C;

const increaseStats = (totalFieldName, mapFieldName) => (model, items) => {
  model.stats[totalFieldName] += items.length;

  items.forEach(item => {
    const { text, count } = item;

    const key = text.replace(/\./g, '_').replace(/^\$/g, '_$');
    const current = model.stats[mapFieldName][key] || 0;
    const inc = count || 1;

    model.stats[mapFieldName][key] = current + inc;
  });
};

const increaseKeywordsStats = increaseStats('totalKeywords', 'keywords');
const increaseEntitiesStats = increaseStats('totalEntities', 'entities');

// Refactor for models
async function saveAnalyzed(model, instance, tweets = []) {
  if (tweets && tweets.length > 0) {
    instance.stats.totalTweets += tweets.length;
    tweets.forEach(({ tweet, nlu }, index) => {
      if (nlu.keywords && nlu.keywords.length > 0) {
        increaseKeywordsStats(instance, nlu.keywords);
      }

      if (nlu.entities && nlu.entities.length > 0) {
        increaseEntitiesStats(instance, nlu.entities);
      }

      const date = moment(
        tweet.created_at,
        'ddd MMM DD HH:mm:ss ZZ YYYY',
      ).toDate();

      tweets[index].created_at = date;
    });
  }

  const { lastTweetId, stats } = instance.toObject();

  const toBeUpdated = {
    lastTweetId,
    stats,
    author: instance.author,
    tweetId: instance.tweetId,
    text: instance.text,
  };
  if (tweets && tweets.length > 0) {
    toBeUpdated['$push'] = {
      tweets: { $each: tweets },
    };
  }

  return model.updateOne(
    {
      _id: instance._id,
    },
    toBeUpdated,
    {
      upsert: true,
    },
  );
}

async function processTweetComments(tweetCommentsStats) {
  const sinceId = tweetCommentsStats.lastTweetId || tweetCommentsStats.tweetId;

  let keepFirstTweetId, tweets, firstPage, maxId;
  let noMoreTweets = false;
  do {
    tweets = await twitterLogic.getTweetComments({
      author: tweetCommentsStats.author,
      sinceId,
      maxId,
    });

    if (tweets && tweets.length > 0) {
      if (keepFirstTweetId && keepFirstTweetId === tweets[0].id_str) {
        throw new Error('Endless loop');
      }

      keepFirstTweetId = tweets[0].id_str;
      const lastTweetId = tweets[tweets.length - 1].id_str;

      maxId = decrementHugeNumberBy1(lastTweetId); // NOTE: https://developer.twitter.com/en/docs/tweets/timelines/guides/working-with-timelines.html

      const comments = twitterLogic.getCommments(
        tweetCommentsStats.tweetId,
        tweets,
      );

      if (!firstPage) {
        // NOTE: Mientras pagina tiene que quedarse con el último hasta que termina, que tiene que quedarse con el primero
        firstPage = true;
        const firstTweetId = tweets[0].id_str;
        tweetCommentsStats.lastTweetId = firstTweetId;
      }

      // TODO: Console.log to logger
      if (comments && comments.length > 0) {
        const analyzed = await nlu.analyzeTweets(comments);

        console.log(`Analyzed ${analyzed.length} comments`);
        await saveAnalyzed(TweetCommentsStats, tweetCommentsStats, analyzed);
      } else {
        console.log('No comments for this page');
      }

      console.log('--- Next page ---');
    } else {
      console.log('--- No tweets retrieved ---');
      noMoreTweets = true;
    }
  } while (!noMoreTweets);
}

async function processTweets(twitterTextSearchStats) {
  const sinceId = twitterTextSearchStats.lastTweetId;

  let keepFirstTweetId, tweets, firstPage, maxId;
  let noMoreTweets = false;
  let pages = 0;
  do {
    tweets = await twitterLogic.getTweetsByText({
      text: twitterTextSearchStats.text,
      sinceId,
      maxId: maxId,
    });

    if (tweets && tweets.length > 0) {
      if (keepFirstTweetId && keepFirstTweetId === tweets[0].id_str) {
        throw new Error('Endless loop');
      }

      keepFirstTweetId = tweets[0].id_str;
      const lastTweetId = tweets[tweets.length - 1].id_str;

      maxId = decrementHugeNumberBy1(lastTweetId); // NOTE: https://developer.twitter.com/en/docs/tweets/timelines/guides/working-with-timelines.html

      if (!firstPage) {
        // NOTE: Mientras pagina tiene que quedarse con el último hasta que termina, que tiene que quedarse con el primero
        firstPage = true;
        const firstTweetId = tweets[0].id_str;
        twitterTextSearchStats.lastTweetId = firstTweetId;
      }

      // TODO: Console.log to logger
      if (tweets && tweets.length > 0) {
        const analyzed = await nlu.analyzeTweets(tweets);

        console.log(`Analyzed ${analyzed.length} tweets`);
        await saveAnalyzed(
          TwitterTextSearchStats,
          twitterTextSearchStats,
          analyzed,
        );
      } else {
        console.log('No tweets for this page');
      }

      console.log('--- Next page ---');
    } else {
      console.log('--- No tweets retrieved ---');
      noMoreTweets = true;
    }
    pages++;
  } while (!noMoreTweets && pages < MAX_PAGES);
  if (pages === MAX_PAGES) {
    console.log(`--- Reached max pages: ${MAX_PAGES} ---`);
  }

  console.log(`--- Finished ---`);
}

function formatCommentsResponse(model, tweets = []) {
  const { author, tweetId, stats } = model.toObject();

  // Refactor tweets mapping
  const data = tweets.map(({ tweet, nlu, created_at }) => ({
    // country, // NOTE: Use google maps to locate or estimate country
    date: created_at,
    sentiment:
      nlu.sentiment && nlu.sentiment.document ? nlu.sentiment.document : {},
    emotion:
      nlu.emotion && nlu.emotion.document && nlu.emotion.document.emotion
        ? nlu.emotion.document.emotion
        : {},
    entities: nlu.entities,
    id_str: tweet.id_str,
    keywords: nlu.keywords,
    categories: nlu.categories,
    semanticRoles: nlu.semantic_roles,
    text: tweet.full_text,
    url: twitterLogic.getTweetUrl(tweet),
    user: tweet.user,
  }));

  return {
    author,
    tweetId,
    statistics: stats,
    comments: data,
  };
}

function formatSearchTextResponse(model, tweets = []) {
  const { text, stats } = model.toObject();

  // Refactor tweets mapping
  const data = tweets.map(({ tweet, nlu, created_at }) => ({
    // country, // NOTE: Use google maps to locate or estimate country
    date: created_at,
    sentiment:
      nlu.sentiment && nlu.sentiment.document ? nlu.sentiment.document : {},
    emotion:
      nlu.emotion && nlu.emotion.document && nlu.emotion.document.emotion
        ? nlu.emotion.document.emotion
        : {},
    entities: nlu.entities,
    categories: nlu.categories,
    id_str: tweet.id_str,
    keywords: nlu.keywords,
    semanticRoles: nlu.semantic_roles,
    text: tweet.full_text,
    url: twitterLogic.getTweetUrl(tweet),
    user: tweet.user,
  }));

  return {
    text,
    statistics: stats,
    comments: data,
  };
}

// TODO: Refactor into models
async function getTweetStatsPage(model, _id) {
  return model.aggregate([
    {
      $match: {
        _id,
      },
    },
    {
      $unwind: {
        path: '$tweets',
        preserveNullAndEmptyArrays: false,
      },
    },
    {
      $sort: { 'tweets.created_at': -1 },
    },
    { $skip: 0 },
    { $limit: 10 },
    {
      $group: {
        _id,
        tweets: { $push: '$tweets' },
      },
    },
  ]);
}

async function findAndAnalyzeTweetComments(author, tweetId) {
  const model = await TweetCommentsStats.findOrCreate({
    author,
    tweetId,
  });

  await processTweetComments(model); // TODO: Cambiar

  // TODO: Get by tweetStats id
  const res = await getTweetStatsPage(TweetCommentsStats, model._id);
  const tweets = res && res.length > 0 ? res[0].tweets : [];

  return formatCommentsResponse(model, tweets);
}

async function findAndAnalyzeTweetsByText(text) {
  const model = await TwitterTextSearchStats.findOrCreate({
    text,
  });

  await processTweets(model); // TODO: Cambiar

  const res = await getTweetStatsPage(TwitterTextSearchStats, model._id);
  const tweets = res && res.length > 0 ? res[0].tweets : [];

  return formatSearchTextResponse(model, tweets);
}

module.exports = {
  findAndAnalyzeTweetComments,
  findAndAnalyzeTweetsByText,
};

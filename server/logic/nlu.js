const NaturalLanguageUnderstandingV1 = require('watson-developer-cloud/natural-language-understanding/v1.js');
const C = require('../../constants');

const url = process.env.WATSON_NLU_API_URL;
const username = process.env.WATSON_NLU_USERNAME;
const password = process.env.WATSON_NLU_PASSWORD;

const { NLU_VERSION } = C;

const nlu = new NaturalLanguageUnderstandingV1({
  version: NLU_VERSION,
  url,
  username,
  password,
});

const analyze = (text, language = 'en') => {
  return new Promise((resolve, reject) => {
    const body = {
      text,
      features: {
        concepts: {},
        entities: {},
        keywords: {},
        categories: {},
        emotion: {},
        sentiment: {},
        semantic_roles: {},
      },
      language,
    };
    nlu.analyze(body, (err, results) => {
      if (err) {
        console.log('REJECTED', `${body.language}: ${text}`);
        return reject(err);
      }
      return resolve(results);
    });
  });
};

async function analyzeTweets(tweets) {
  const toBeSent = [];
  await Promise.all(
    tweets.map(async tweet => {
      try {
        const lang = tweet.lang !== 'und' ? tweet : tweet.user.lang;
        const analyzed = await analyze(tweet.full_text, lang);

        toBeSent.push({ tweet, nlu: analyzed });
      } catch (e) {
        // console.log(e);
      }
    }),
  );

  return toBeSent;
}

module.exports = {
  analyzeTweets,
};

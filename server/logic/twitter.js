const Twit = require('twit');

const C = require('../../constants');

// 'ddd MMM DD HH:mm:ss ZZ YYYY'

const configTwitter = {
  consumer_key: process.env.TWITTER_CONSUMER_KEY,
  consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
  access_token: process.env.TWITTER_ACCESS_TOKEN,
  access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET,
};

const api = new Twit(configTwitter);

function getTweetUrl({ user: { screen_name, id_str } }) {
  return `https://twitter.com/${screen_name}/status/${id_str}`;
}

function getTweetText(tweet) {
  const text = tweet.retweeted_status
    ? tweet.retweeted_status.full_text
    : tweet.full_text;

  return text
    .split(/ |\n/)
    .filter(word => !word.startsWith('http'))
    .join(' ');
}

function getCommments(tweetId, tweets) {
  return tweets.filter(tweet => tweet.in_reply_to_status_id_str === tweetId);
}

async function getTweetComments({ author, sinceId, maxId }) {
  const options = {
    q: `to:@${author}`,
    since_id: sinceId,
  };

  if (maxId) options.max_id = maxId;

  return getTweets(options);
}

async function getTweetsByText({ text, sinceId, maxId }) {
  const options = {
    q: text,
  };

  if (sinceId) options.since_id = sinceId;
  if (maxId) options.max_id = maxId;

  return getTweets(options);
}

async function getTweets(extraOptions) {
  const options = {
    count: C.TWEET_LIMIT,
    include_entities: C.TWEET_INCLUDE_ENTITIES,
    result_type: C.TWEET_RESULT_TYPE,
    tweet_mode: C.TWEET_MODE,
    ...extraOptions,
  };

  const tweets = await api.get(C.TWITTER_SEARCH_TWEETS_ENDPOINT, options);

  return tweets.data.statuses;
}

module.exports = {
  getCommments,
  getTweetComments,
  getTweets,
  getTweetsByText,
  getTweetText,
  getTweetUrl,
};

const jwt = require('jsonwebtoken');

const User = require('../models/User');

const { SECRET_KEY, SESSION_EXPIRES } = process.env;

async function register(email, password) {
  const user = new User({
    email,
    password,
  });

  // save the sample user
  await user.save();

  return user.toObject();
}

async function login(email, password) {
  // find the user
  const user = await User.findOne({ email });

  if (!user || user.password != password) {
    return;
  }

  const payload = {
    email: user.email,
  };

  var token = jwt.sign(payload, SECRET_KEY, {
    expiresInMinutes: SESSION_EXPIRES,
  });

  return token;
}

function verify() {}

module.exports = {
  login,
  register,
  verify,
};

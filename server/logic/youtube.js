const gapis = require('googleapis');
const google = gapis.google;

function configYoutube() {
  return google.youtube({
    version: 'v3',
    auth: process.env.YOUTUBE_AUTH,
  });
}

function pickUsefulData(youtubeComment) {
  const {
    id,
    snippet: {
      topLevelComment: {
        snippet: {
          authorDisplayName,
          textOriginal,
          likeCount,
          publishedAt,
          updatedAt,
        },
      },
    },
  } = youtubeComment;

  return {
    id,
    authorDisplayName,
    textOriginal,
    likeCount,
    publishedAt,
    updatedAt,
  };
}

async function getCommentThreads() {
  const youtube = configYoutube();
  const comments = await youtube.commentThreads.list({
    part: 'snippet',
    videoId: '64VqMplcpkM',
    maxCount: 100,
  });

  return comments.data.items.map(comment => pickUsefulData(comment));
}

module.exports = getCommentThreads;

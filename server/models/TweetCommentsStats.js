const mongoose = require('mongoose');

const TweetCommentsStatsSchema = new mongoose.Schema({
  author: String,
  tweetId: String,
  lastTweetId: String,
  stats: {
    totalTweets: { type: Number, default: 0 },
    totalKeywords: { type: Number, default: 0 },
    totalEntities: { type: Number, default: 0 },
    keywords: { type: mongoose.Schema.Types.Mixed, default: {} },
    entities: { type: mongoose.Schema.Types.Mixed, default: {} },
  },
  tweets: { type: Array, default: [] }, // tweet y nlu
});

TweetCommentsStatsSchema.statics.findOrCreate = function({ author, tweetId }) {
  const Model = this.model('TweetCommentsStats');
  return Model.findOne({ author, tweetId }, '-tweets')
    .exec()
    .then(tweetStats => {
      if (!tweetStats) {
        tweetStats = new Model({ author, tweetId });
      }

      return tweetStats;
    });
};

module.exports = mongoose.model('TweetCommentsStats', TweetCommentsStatsSchema);

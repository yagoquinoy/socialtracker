const mongoose = require('mongoose');

// NOTE: Refactor Schema
const TwitterTextSearchStatsSchema = new mongoose.Schema({
  text: String,
  lastTweetId: String, // NOTE: Are text search deterministics?
  stats: {
    totalTweets: { type: Number, default: 0 },
    totalKeywords: { type: Number, default: 0 },
    totalEntities: { type: Number, default: 0 },
    keywords: { type: mongoose.Schema.Types.Mixed, default: {} },
    entities: { type: mongoose.Schema.Types.Mixed, default: {} },
  },
  tweets: { type: Array, default: [] }, // tweet y nlu
});

TwitterTextSearchStatsSchema.statics.findOrCreate = function({ text }) {
  const Model = this.model('TwitterTextSearchStats');
  return Model.findOne({ text }, '-tweets')
    .exec()
    .then(tweetStats => {
      if (!tweetStats) {
        tweetStats = new Model({ text });
      }

      return tweetStats;
    });
};

module.exports = mongoose.model(
  'TwitterTextSearchStats',
  TwitterTextSearchStatsSchema,
);

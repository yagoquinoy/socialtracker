const mongoose = require('mongoose');

// NOTE: Refactor Schema
const UserSchema = new mongoose.Schema({
  email: String,
  password: String,
});

module.exports = mongoose.model('User', UserSchema);

const analyzeController = require('./controllers/analyze');
const userController = require('./controllers/user');

module.exports = function({ app, handle, server }) {
  // Pages
  server.get('/', (req, res) => app.render(req, res, '/', req.query));
  server.get('/register', (req, res) =>
    app.render(req, res, '/register', req.query),
  );
  server.get('/verify', (req, res) =>
    app.render(req, res, '/twofactor', req.query),
  );
  server.get('/twitter', (req, res) =>
    app.render(req, res, '/twitter', req.query),
  );

  // Api endpoints
  server.post('/api/twitter', analyzeController.twitter);
  server.post('/api/register', userController.register);
  server.post('/api/login', userController.login);
  server.post('/api/login/verify', userController.verify);

  server.get('*', (req, res) => handle(req, res));
};

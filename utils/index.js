export function sorter(words) {
  const wordEntries = Object.keys(words).map(key => [key, words[key]]);

  const sortedKeys = wordEntries.sort((entry1, entry2) => {
    if (entry1[1] > entry2[1]) {
      return -1;
    }

    if (entry1[1] < entry2[1]) {
      return 1;
    }

    return 0;
  });

  return sortedKeys;
}

export default {
  sorter,
};
